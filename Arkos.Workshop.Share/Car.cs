﻿using System.Collections.Generic;
namespace Arkos.Data
{
    public class Car
    {
        public int Id { get; set; }
        
        public string Brand { get; set; }
        
        public string Model { get; set; }
        
        public string Plate { get; set; }

        public bool? IsActive { get; set; }

        public List<Fueling> Fuelings { get; set; }
    }
}
