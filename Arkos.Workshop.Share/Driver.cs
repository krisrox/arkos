﻿namespace Arkos.Data
{
    using System.Collections.Generic;

    public class Driver
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public string LastName { get; set; }

        public bool? IsActive { get; set; }

        public List<Fueling> Fuelings { get; set; }
    }
}
