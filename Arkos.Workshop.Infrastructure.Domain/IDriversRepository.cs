﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Arkos.Data;

namespace Arkos.Workshop.Infrastructure.Domain
{
    public interface IDriversRepository
    {
        Task<ICollection<Driver>> GetAll(bool includeFuel);

        Task<Driver> FindById(int id, bool includeFuel);

        Task<Driver> Create(Driver item);

        Task<Driver> Update(int id, Driver item);

        Task<Driver> Delete(int id);
    }
}