﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Arkos.Data;

namespace Arkos.Workshop.Infrastructure.Domain
{
    public interface ICarsRepository
    {
        Task<IList<Car>> GetAll(bool includeAll = true);

        Task<Car> FindById(int id, bool includeAll = true);

        Task<Car> Create(Car item);

        Task<Car> Update(int id, Car item);
    }
}