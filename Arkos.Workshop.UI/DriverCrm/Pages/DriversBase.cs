﻿using Arkos.Workshop.UI.DriverCrm.ViewModels;
using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using System.Threading.Tasks;
using Arkos.Workshop.UI.DriverCrm.Services.Domain;
using System;
using MatBlazor;
using Microsoft.AspNetCore.Components.Forms;
using System.IO;

namespace Arkos.Workshop.UI.DriverCrm.Pages
{
    public class DriversBase : ComponentBase
    {
        [Inject]
        public IDriverService DriverService { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        protected IMatToaster Toaster { get; set; }

        [Parameter]
        public string DriverId { get; set; }

        public DriverViewModel Driver { get; set; } = new DriverViewModel();

        public IList<DriverViewModel> Drivers { get; set; }

        protected string SearchStringToFilter { get; set; }

        protected IList<string> ImageDataUrls = new List<string>();

        protected IReadOnlyList<IBrowserFile> SelectedImage;

        protected bool DialogIsOpen;

        private bool _isEdit;

        protected bool Saved;

        protected bool SuccessValidation;

        protected string ExceptionMessage;

        protected override async Task OnInitializedAsync()
        {
            Drivers = await DriverService.GetAll(true);
        }

        protected async Task OpenDialog(bool isEdit, int driverId = 0)
        {
            Saved = false;
            _isEdit = isEdit;
            if (!isEdit)
            {
                Driver = new DriverViewModel { Id = 0 };
            }
            else
            {
                Driver = await DriverService.GetById(driverId);
            }
            DialogIsOpen = true;
        }

        protected async Task ConfirmActionIntoDialog()
        {
            DialogIsOpen = false;
            if (!_isEdit)
            {
                var addedDriver = await DriverService.Create(Driver);

                if (addedDriver != null)
                {
                    
                    FileUpload(addedDriver);
                    Toaster.Add("Dodano pomyślnie!", MatToastType.Success, "Sukces");
                    Saved = true;
                    if (addedDriver?.Id > 0)
                    {
                        Driver = addedDriver;
                    }
                }
                else
                {
                    Toaster.Add("Coś poszło nie tak. Spróbuj ponownie!", MatToastType.Danger, "Błąd");
                    Saved = false;
                }
            }
            else
            {
                await DriverService.Update(Driver);
                FileUpload(Driver);
                Toaster.Add("Zaaktualizowano pomyślnie!", MatToastType.Success, "Sukces");
                Saved = true;
            }
        }

        protected void CloseDialog() => DialogIsOpen = false;

        public bool FilterData(DriverViewModel element)
        {
            if (string.IsNullOrWhiteSpace(SearchStringToFilter)) return true;
            if (element.Name.Contains(SearchStringToFilter, StringComparison.OrdinalIgnoreCase)) return true;
            if (element.LastName.Contains(SearchStringToFilter, StringComparison.OrdinalIgnoreCase)) return true;
            return false;
        }

        protected async Task OnChange(InputFileChangeEventArgs e)
        {
            ExceptionMessage = string.Empty;

            try
            {
                var imageFiles = e.GetMultipleFiles(1);
                SelectedImage = imageFiles;
                var format = "image/png";
                foreach (var file in imageFiles)
                {
                    var resizedFile = await file.RequestImageFileAsync(format, 300, 300);
                    var buffer = new byte[resizedFile.Size];
                    await resizedFile.OpenReadStream().ReadAsync(buffer);
                    var imageDataUrl = $"data:{format};base64,{Convert.ToBase64String(buffer)}";
                    if (file.Size >= 512000)
                    {
                        Toaster.Add("Zdjęcie posiada za duży rozmiar!", MatToastType.Danger, "Błąd");
                    }
                    else
                    {
                        ImageDataUrls.Add(imageDataUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage = ex.Message;

            }
        }

        protected async void FileUpload(DriverViewModel newValue)
        {
            if (SelectedImage != null)
            {
                foreach (var file in SelectedImage)
                {
                    var newName = file.Name.Replace(file.Name, newValue.Id + ".jpg");
                    var path = $"{Directory.GetCurrentDirectory()}{@"\wwwroot\images\DriversAvatar\"}{newName}";

                    Stream stream = file.OpenReadStream();
                    FileStream fs = File.Create(path);
                    await stream.CopyToAsync(fs);
                    fs.Close();
                }
            }

            StateHasChanged();
        }
    }
}
