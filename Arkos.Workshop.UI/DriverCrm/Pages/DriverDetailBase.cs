﻿using Arkos.Workshop.UI.DriverCrm.ViewModels;
using Microsoft.AspNetCore.Components;
using System.Threading.Tasks;
using Arkos.Workshop.UI.DriverCrm.Services.Domain;
using System;
using System.IO;
using MudBlazor;

namespace Arkos.Workshop.UI.DriverCrm.Pages
{
    public class DriverDetailBase : ComponentBase
    {
        [Inject]
        public IDriverService DriverService { get; set; }

        [Parameter]
        public int DriverId { get; set; }

        public DriverViewModel Driver { get; set; } = new DriverViewModel();

        private readonly string _driverImagePath = $"{Directory.GetCurrentDirectory()}{@"\wwwroot\images\DriversAvatar\"}";

        protected string DriverImage = String.Empty;

        public readonly string ArkosColors = $"color:{Colors.Shades.White};background-color:{Colors.Blue.Accent4};";

        protected override async Task OnInitializedAsync()
        {
            Driver = await DriverService.GetById(DriverId);
        }

        protected void ReadImage(DriverViewModel objectId)
        {
            if (File.Exists(_driverImagePath + objectId.Id + ".jpg"))
            {
                DriverImage = "/images/DriversAvatar/" + objectId.Id + ".jpg";
            }
            else
            {
                DriverImage = "/images/DriversAvatar/" + "blank.jpg";
            }
                
        }
    }
}