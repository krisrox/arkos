﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Arkos.Workshop.UI.DriverCrm.ViewModels;

namespace Arkos.Workshop.UI.DriverCrm.Services.Domain
{
    public interface IDriverService
    {
        Task<DriverViewModel> Create(DriverViewModel item);
        Task<IList<DriverViewModel>> GetAll(bool includeAll = true);
        Task<DriverViewModel> GetById(int id);
        Task Update(DriverViewModel item);
    }
}
