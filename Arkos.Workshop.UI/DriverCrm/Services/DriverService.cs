﻿using Arkos.Workshop.UI.DriverCrm.ViewModels;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Arkos.Workshop.UI.DriverCrm.Services.Domain;

namespace Arkos.Workshop.UI.DriverCrm.Services
{
    public class DriverService : IDriverService
    {
        private readonly HttpClient _httpClient;

        public DriverService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IList<DriverViewModel>> GetAll(bool includeAll = true)
        {
            string relativeUri = $"?includeAll={includeAll}";
            var uri = new System.Uri(_httpClient.BaseAddress, relativeUri);
            return await JsonSerializer.DeserializeAsync<IList<DriverViewModel>>(
                await _httpClient.GetStreamAsync(uri),
                new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
        }

        public async Task<DriverViewModel> GetById(int id)
        {
            string relativeUri = $"Drivers/{id}";
            return await JsonSerializer.DeserializeAsync<DriverViewModel>(
                await _httpClient.GetStreamAsync(relativeUri),
                new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
        }

        public async Task<DriverViewModel> Create(DriverViewModel item)
        {
            string relativeUri = $"Drivers";
            var bodyJson =
                new StringContent(JsonSerializer.Serialize(item),
                    Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(relativeUri, bodyJson);

            if (response.IsSuccessStatusCode)
            {
                return await JsonSerializer.DeserializeAsync<DriverViewModel>(
                    await response.Content.ReadAsStreamAsync(),
                    new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            }

            return null;
        }

        public async Task Update(DriverViewModel item)
        {
            string relativeUri = $"Drivers/{item.Id}";
            var bodyJson =
                new StringContent(JsonSerializer.Serialize(item),
                    Encoding.UTF8, "application/json");

            await _httpClient.PutAsync(relativeUri, bodyJson);
        }
    }
}

