﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(Arkos.Workshop.UI.Areas.Identity.IdentityHostingStartup))]
namespace Arkos.Workshop.UI.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
    }
}