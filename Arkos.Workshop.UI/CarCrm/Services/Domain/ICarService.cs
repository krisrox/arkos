﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Arkos.Workshop.UI.CarCrm.ViewModels;

namespace Arkos.Workshop.UI.CarCrm.Services.Domain
{
    public interface ICarService
    {
        Task<CarViewModel> Create(CarViewModel item);
        Task<IList<CarViewModel>> GetAll(bool includeAll = true);
        Task<CarViewModel> GetById(int id);
        Task Update(CarViewModel item);
    }
}
