﻿using Microsoft.AspNetCore.Components;
using Arkos.Workshop.UI.CarCrm.ViewModels;
using System.Threading.Tasks;
using Arkos.Workshop.UI.CarCrm.Services.Domain;

namespace Arkos.Workshop.UI.CarCrm.Pages
{
    public class CarDetailBase : ComponentBase
    {
        [Inject]
        public ICarService CarService { get; set; }

        [Parameter]
        public int CarId { get; set; }
        public CarViewModel Car { get; set; } = new CarViewModel();

        protected override async Task OnInitializedAsync()
        {
            Car = await CarService.GetById(CarId);
        }
    }
}
