﻿using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using System.Threading.Tasks;
using Arkos.Workshop.UI.CarCrm.Services.Domain;
using Arkos.Workshop.UI.CarCrm.ViewModels;
using System;
using MatBlazor;

namespace Arkos.Workshop.UI.CarCrm.Pages
{
    public class CarsBase : ComponentBase
    {   
        [Inject]
        public ICarService CarService { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        protected IMatToaster Toaster { get; set; }

        [Parameter]
        public string CarId { get; set; }

        public CarViewModel Car { get; set; } = new CarViewModel();

        public IList<CarViewModel> Cars { get; set; }

        protected string SearchStringToFilter { get; set; }

        protected bool DialogIsOpen;

        private bool _isEdit;

        protected bool SuccessValidation;

        protected bool Saved;

        protected override async Task OnInitializedAsync()
        {
            Cars = await CarService.GetAll(true);
        }

        protected async Task OpenDialog(bool isEdit, int carId = 0)
        {
            Saved = false;
            _isEdit = isEdit;
            if (!isEdit)
            {
                Car = new CarViewModel { Id = 0 };
            }
            else
            {
                Car = await CarService.GetById(carId);
            }
            DialogIsOpen = true;
        }

        protected async Task ConfirmActionIntoDialog()
        {
            DialogIsOpen = false;
            if (!_isEdit)
            {
                var addedCar = await CarService.Create(Car);
                
                if (addedCar != null)
                {
                    Toaster.Add("Dodano pomyślnie!", MatToastType.Success, "Sukces");
                    Saved = true;
                    if (addedCar?.Id > 0)
                    {
                        Car = addedCar;
                    }
                }
                else
                {
                    Toaster.Add("Coś poszło nie tak. Spróbuj ponownie!", MatToastType.Danger, "Błąd");
                    Saved = false;
                }
            }
            else
            {   
                await CarService.Update(Car);
                Toaster.Add("Zaaktualizowano pomyślnie!", MatToastType.Success, "Sukces");
                Saved = true;
            }
        }

        protected void CloseDialog() => DialogIsOpen = false;

        public bool FilterData(CarViewModel element)
        {
            if (string.IsNullOrWhiteSpace(SearchStringToFilter)) return true;
            if (element.Brand.Contains(SearchStringToFilter, StringComparison.OrdinalIgnoreCase)) return true;
            if (element.Model.Contains(SearchStringToFilter, StringComparison.OrdinalIgnoreCase)) return true;
            if (element.Plate.Contains(SearchStringToFilter, StringComparison.OrdinalIgnoreCase)) return true;
            return false;
        }
    }
}
