﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Arkos.Workshop.UI.FuelingCrm.ViewModels;

namespace Arkos.Workshop.UI.FuelingCrm.Services.Domain
{
    public interface IFuelingService
    {   
        Task<FuelingViewModel> Create(int carId, int driverId, FuelingViewModel item);

        Task<ICollection<FuelingViewModel>> GetAll(bool includeAll = true);

        Task Update(int carId, int driverId, FuelingViewModel item);

        Task<FuelingViewModel> GetById(int id);

        Task<IList<FuelingViewModel>> GetByCarId(int carId);

        Task<IList<FuelingViewModel>> GetByDriverId(int driverId);
    }
}
