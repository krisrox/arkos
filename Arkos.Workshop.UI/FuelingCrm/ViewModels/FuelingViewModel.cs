﻿using Arkos.Data;
using System;

namespace Arkos.Workshop.UI.FuelingCrm.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    public class FuelingViewModel
    {
        public int Id { get; set; }
        [Required]
        [MinLength(1)]
        public Car Car { get; set; }
        [Required]
        public Driver Driver { get; set; }
        [Required]
        public double Fuel { get; set; }
        [Required]
        public double Cost { get; set; }
        [Required]
        public int RefuelingMileage { get; set; }
        [Required]
        public DateTime? Date { get; set; } = DateTime.Now;
    }
}
