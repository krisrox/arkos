﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Arkos.Data;
using Arkos.Workshop.Infrastructure.Domain;
using Microsoft.EntityFrameworkCore;

namespace Arkos.Workshop.Infrastructure
{
    public class DriversRepository : IDriversRepository
    {
        private readonly ArkosWorkshopContext _arkosContext;

        public DriversRepository(ArkosWorkshopContext context)
        {
            _arkosContext = context;
        }

        public async Task<Driver> Create(Driver item)
        {
            var ret = _arkosContext.Drivers.Add(item);
            await _arkosContext.SaveChangesAsync();
            return ret.Entity;
        }

        public async Task<Driver> FindById(int id, bool includeFuel)
        {
            if (includeFuel)
            {
                return await _arkosContext.Drivers
                    .Include(q => q.Fuelings)
                    .ThenInclude(q => q.Car)
                    .FirstOrDefaultAsync(q => q.Id == id);
            }
            else
            {
                return await _arkosContext.Drivers.FindAsync(id);
            }
        }

        public async Task<Driver> Update(int id, Driver item)
        {
            if (item is null)
            {
                return null;
            }

            var exist = await FindById(id, false);
            if (exist != null)
            {
                _arkosContext.Entry(exist).CurrentValues.SetValues(item);
                _arkosContext.SaveChanges();
            }
            return exist;
        }

        public async Task<ICollection<Driver>> GetAll(bool includeFuel)
        {
            if (includeFuel)
            {
                return
                    await _arkosContext
                        .Drivers
                        .Include(q => q.Fuelings)
                        .ToListAsync();
            }
            else
            {
                return
                    await _arkosContext
                        .Drivers
                        .ToListAsync();
            }
        }

        public async Task<Driver> Delete(int id)
        {
            var del = await FindById(id, false);
            if (del != null)
            {
                _arkosContext.Remove(del);
                _arkosContext.SaveChanges();
            }
            return del;
        }
    }
}
