﻿using Microsoft.EntityFrameworkCore;
using Arkos.Data;

namespace Arkos.Workshop.Infrastructure
{
    public class ArkosWorkshopContext : DbContext
    {
        public ArkosWorkshopContext(DbContextOptions<ArkosWorkshopContext> options) : base(options)
        {
        }
        public DbSet<Car> Cars { get; set; } //obiekty do tworzenia tabeli

        public DbSet<Driver> Drivers { get; set; }

        public DbSet<Fueling> Fuelings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
