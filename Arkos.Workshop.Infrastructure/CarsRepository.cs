﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Arkos.Data;
using Arkos.Workshop.Infrastructure.Domain;
using Microsoft.EntityFrameworkCore;

namespace Arkos.Workshop.Infrastructure
{
    public class CarsRepository : ICarsRepository
    {
        private readonly ArkosWorkshopContext _arkosContext;

        public CarsRepository(ArkosWorkshopContext context)
        {
            _arkosContext = context;
        }

        public async Task<Car> Create(Car item)
        {
            var ret = _arkosContext.Cars.Add(item);
            await _arkosContext.SaveChangesAsync();
            return ret.Entity;
        }

        public async Task<Car> FindById(int id, bool includeFuel)
        {
            if (includeFuel)
            {
                return await _arkosContext.Cars
                    .Include(q => q.Fuelings)
                    .FirstOrDefaultAsync(q => q.Id == id);
            }
            else
            {
                return await _arkosContext.Cars.FindAsync(id);
            }
        }

        public async Task<IList<Car>> GetAll(bool includeFuel)
        {
            if (includeFuel)
            {
                return
                    await _arkosContext
                        .Cars
                        .Include(q => q.Fuelings)
                        .ToListAsync();
            }
            else
            {
                return
                    await _arkosContext
                        .Cars
                        .ToListAsync();
            }
        }

        public async Task<Car> Update(int id, Car item)
        {
            if (item is null)
            {
                return null;
            }

            var exist = await FindById(id, false);
            if (exist != null)
            {
                _arkosContext.Entry(exist).CurrentValues.SetValues(item);
                _arkosContext.SaveChanges();
            }
            return exist;
        }
    }
}
