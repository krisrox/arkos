﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Arkos.Workshop.Infrastructure.Domain;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Arkos.Data;
using Arkos.Workshop.Api.DataDto;

namespace Arkos.Workshop.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DriversController : ControllerBase
    {
        private readonly IDriversRepository _repository;
        private readonly ILogger<DriversController> _logger;

        public DriversController(ILogger<DriversController> logger, IDriversRepository repository)
        {
            _logger = logger;
            _repository = repository;
        }

        [HttpGet]
        public async Task<ActionResult<IList<Driver>>> GetAll(bool includeAll)
        {
            return Ok(await _repository.GetAll(includeAll));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Driver>> Get(int id, bool includeAll)
        {
            return Ok(await _repository.FindById(id, includeAll));
        }

        [HttpPost]
        public async Task<ActionResult<Driver>> Create(DriverDto item)
        {
            var nq = new Driver
            {
                Id = item.Id,
                Name = item.Name,
                LastName = item.LastName, 
                IsActive = item.IsActive
            };

            var created = await _repository.Create(nq);
            return CreatedAtAction(
                nameof(this.Get),
                new { id = created.Id },
                created);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, DriverDto item)
        {
            if (id != item?.Id)
            {
                return BadRequest();
            }

            var na = new Driver
            {
                Id = item.Id,
                Name = item.Name,
                LastName = item.LastName,
                IsActive = item.IsActive
            };

            var updated = await _repository.Update(id, na);
            if (updated == null)
            {
                return NotFound();
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var deleted = await _repository.Delete(id);
            if (deleted == null)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
