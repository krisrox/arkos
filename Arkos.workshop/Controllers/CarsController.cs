﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Arkos.Workshop.Infrastructure.Domain;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Arkos.Data;
using Arkos.Workshop.Api.DataDto;

namespace Arkos.Workshop.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CarsController : ControllerBase
    {
        private readonly ICarsRepository _repository;
        private readonly ILogger<CarsController> _logger;

        public CarsController(ILogger<CarsController> logger, ICarsRepository repository)
        {
            _logger = logger;
            _repository = repository;
        }

        [HttpGet]
        public async Task<ActionResult<IList<Car>>> GetAll(bool includeAll = true)
        {
            return Ok(await _repository.GetAll(includeAll));
        }

        [HttpGet("{id}", Name = "Get")]
        public async Task<ActionResult<Car>> Get(int id)
        {
            return Ok(await _repository.FindById(id));
        }

        [HttpPost]
        public async Task<ActionResult<Car>> Post(CarDto item)
        {
            var nq = new Car
            {
                Id = item.Id,
                Brand = item.Brand,
                Model = item.Model,
                Plate = item.Plate,
                IsActive = item.IsActive
            };

            var created = await _repository.Create(nq);
            return CreatedAtAction(
                nameof(this.Get),
                new { id = created.Id },
                created);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, CarDto item)
        {
            if (id != item?.Id)
            {
                return BadRequest();
            }

            var uq = new Car
            {
                Id = item.Id,
                Brand = item.Brand,
                Model = item.Model,
                Plate = item.Plate,
                IsActive = item.IsActive
            };

            var updated = await _repository.Update(id, uq);
            if (updated == null)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
