﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Arkos.Workshop.Infrastructure.Domain;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Arkos.Data;
using Arkos.Workshop.Api.DataDto;

namespace Arkos.Workshop.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class FuelingsController : ControllerBase
    {
        private readonly IFuelingsRepository _repository;
        private readonly ILogger<FuelingsController> _logger;

        public FuelingsController(ILogger<FuelingsController> logger, IFuelingsRepository repository)
        {
            _logger = logger;
            _repository = repository;
        }

        [HttpGet]
        public async Task<ActionResult<IList<Fueling>>> GetAll(bool includeAll = true)
        {
            return Ok(await _repository.GetAll(includeAll));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Fueling>> Get(int id, bool includeAll = true)
        {
            return Ok(await _repository.FindById(id, includeAll));
        }

        [HttpGet("find/{carId}")]
        public async Task<ActionResult<List<Fueling>>> FilterByCar(int carId)
        {
            return Ok(await _repository.FindByCarId(carId));
        }

        [HttpGet("find/filter/{driverId}")]
        public async Task<ActionResult<List<Fueling>>> FilterByDriver(int driverId)
        {
            return Ok(await _repository.FindByDriverId(driverId));
        }

        [HttpPost]
        public async Task<ActionResult<Fueling>> Create(int carId, int driverId, FuelingDto item)
        {
            var nq = new Fueling
            {
                Id = item.Id,
                Fuel = item.Fuel,
                Cost = item.Cost,
                RefuelingMileage = item.RefuelingMileage,
                Date = item.Date
            };

            var created = await _repository.Create(carId, driverId, nq);
            return CreatedAtAction(
                nameof(this.Get),
                new { id = created.Id },
                created);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, int carId, int driverId, FuelingDto item)
        {
            if (id != item?.Id)
            {
                return BadRequest();
            }

            var na = new Fueling
            {
                Id = item.Id,
                Fuel = item.Fuel,
                Cost = item.Cost,
                RefuelingMileage = item.RefuelingMileage,
                Date = item.Date
            };

            var updated = await _repository.Update(id, carId, driverId, na);
            if (updated == null)
            {
                return NotFound();
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var deleted = await _repository.Delete(id);
            if (deleted == null)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
