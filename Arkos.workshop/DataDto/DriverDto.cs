﻿namespace Arkos.Workshop.Api.DataDto
{
    public class DriverDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public bool? IsActive { get; set; }
    }
}
